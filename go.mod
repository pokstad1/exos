module gitlab.com/pokstad1/exos

go 1.13

require (
	github.com/gomarkdown/markdown v0.0.0-20191229055418-359dc4b9d66b
	gopkg.in/yaml.v2 v2.2.7
)
