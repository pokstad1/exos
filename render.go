package main

import (
	"bytes"
	"encoding/gob"
	"errors"
	"flag"
	"fmt"
	"go/doc"
	"io"
	"text/template"

	"github.com/gomarkdown/markdown"
)

func render(args []string, src io.Reader, dst io.Writer) error {
	var (
		flags    = flag.NewFlagSet("render", flag.ContinueOnError)
		glob     = flags.String("sources", "", "path glob to Go template sources")
		tmplName = flags.String("template", "", "template name to render")
	)

	verbose("parsing args %v", args)
	if err := flags.Parse(args); err != nil {
		return err
	}

	if *tmplName == "" {
		return errors.New("missing flag for template name to render")
	}

	t, err := template.New("").Funcs(funcMap).ParseGlob(*glob)
	if err != nil {
		return fmt.Errorf("unable to parse templates: %w", err)
	}

	t = t.Lookup(*tmplName)
	if t == nil {
		return fmt.Errorf("template not found: %s", *tmplName)
	}

	verbose("template: %+v", t)

	dec := gob.NewDecoder(src)
	for {
		m := new(msg)
		if err := dec.Decode(m); err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		if err := t.Execute(dst, m.Data); err != nil {
			return err
		}
	}

	return nil
}

var funcMap = template.FuncMap{
	// markdown converts markdown string to HTML
	"markdown": func(md string) string {
		return string(markdown.ToHTML([]byte(md), nil, nil))
	},
	// godocToHtml converts a Go doc comment string to HTML
	"godocToHtml": func(d string) string {
		b := new(bytes.Buffer)
		doc.ToHTML(b, d, nil)
		return b.String()
	},
}
