package main

import (
	"bytes"
	"flag"
	"os/exec"
	"testing"
)

func TestTranslateRender(t *testing.T) {
	flag.Parse() // use '-verbose' flag for more logging

	for _, tt := range []struct {
		tArgs    []string
		template string
		output   string
	}{
		{
			[]string{"-format", "yaml", "-in", "testdata/turtles.yml"},
			"tmnt.template",
			"Mike, Don, Raf, Leo, \n[sardines pepperoni cheese], \n\n",
		},
		{
			[]string{"-format", "json", "-in", "testdata/profile.json"},
			"profile.template",
			"Hello Paul\nHello Toby\n",
		},
		{
			[]string{"-format", "yaml+markdown", "-in", "testdata/article.md"},
			"article.template",
			"best blog ever\n\n[blog computers kettlebells]\n\n<p>This blog is the best damn blog ever for a reason.</p>\n\n",
		},
		{
			[]string{"-format", "raw", "-in", "testdata/raw.md"},
			"raw.template",
			"<h1>This is a title</h1>\n\n<p>This is a body</p>\n\n<ul>\n<li>this is a list</li>\n<li>and so is this</li>\n</ul>\n\n",
		},
		{
			[]string{"-format", "raw", "-in", "testdata/godoc"},
			"godoc.template",
			"<p>\nCommand &#34;exos&#34; is totally dope.\n</p>\n<h3 id=\"hdr-Title_Text\">Title Text</h3>\n<p>\nThis is paragraph.\n</p>\n<pre>This is code.\n</pre>\n\n",
		},
	} {
		t.Run(tt.tArgs[1], func(t *testing.T) {
			b1 := new(bytes.Buffer)
			if err := translate(tt.tArgs, nil, b1); err != nil {
				t.Fatal(err)
			}

			b2 := new(bytes.Buffer)
			rArgs := []string{"-sources", "testdata/*.template", "-template", tt.template}
			if err := render(rArgs, b1, b2); err != nil {
				t.Fatal(err)
			}

			if tt.output != b2.String() {
				t.Logf("expected: %s", tt.output)
				t.Logf("got: %s", b2.String())
				t.Fatal("rendered output doesn't match")
			}
		})
	}
}

func TestReduce(t *testing.T) {
	actual, err := exec.Command("/bin/sh", "-c", "testdata/reduce.sh").Output()
	if err != nil {
		t.Fatal(err)
	}

	expect := `Start
Outer Key: A
Outer Value: map[a:1]
Inner key: a
Inner Value: 1
Outer Key: B
Outer Value: map[b:2]
Inner key: b
Inner Value: 2
End
`

	if string(actual) != expect {
		t.Logf("expected: %s", expect)
		t.Logf("got: %s", string(actual))
		t.Fatal("did not receive expected output")
	}
}
