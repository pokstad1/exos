package main

import (
	"flag"
	"log"
	"os"
)

var verboseLog = flag.Bool("verbose", false, "verbose logging")

func verbose(msg string, args ...interface{}) {
	if *verboseLog {
		log.Printf(msg, args...)
	}
}

func main() {
	flag.Parse()

	if len(flag.Args()) < 1 {
		log.Fatalf("missing subcommand; see usage")
	}

	cmd, args := flag.Args()[0], flag.Args()[1:]

	switch cmd {
	case "render":
		logIfErr(render(args, os.Stdin, os.Stdout))
	case "translate":
		logIfErr(translate(args, os.Stdin, os.Stdout))
	case "reduce":
		logIfErr(reduce(args, os.Stdin, os.Stdout))
	default:
		log.Fatalf("unknown subcommand %q; see usage", cmd)
	}

}

func logIfErr(err error) {
	if err != nil {
		log.Fatalf("unable to execute subcommand: %v", err)
	}
}

type msg struct {
	Label string
	Data  interface{}
}
