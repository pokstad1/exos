#!/bin/sh

# Exit with non-zero if any command fails
set -e

go install
export PATH=$PATH:$GOPATH/bin

{
	echo '{"a":1}' | exos -verbose translate -format json -label A; 
	echo '{"b":2}' | exos -verbose translate -format json -label B;
} |
	exos -verbose reduce map |
	exos -verbose render -sources 'testdata/*.template' -template reduced.template



