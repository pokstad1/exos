
DEFAULT_GOAL := default
default: public/index.html

EXOS:=$(GOPATH)/bin/exos

$(EXOS): *.go
	go install gitlab.com/pokstad1/exos

public/index.html: README.md examples/website/index.html.template $(EXOS)
	cat README.md | \
		$(EXOS) translate -format raw | \
		$(EXOS) render -sources 'examples/website/*.template' -template index.html.template > \
		public/index.html
