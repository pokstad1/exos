package main

import (
	"encoding/gob"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"reflect"

	yaml "gopkg.in/yaml.v2"
)

// translate takes various source formats and converts them to a universal
// medium (gob format) to be used in templates
func translate(args []string, src io.Reader, dst io.Writer) error {
	var (
		flags  = flag.NewFlagSet("translate", flag.ContinueOnError)
		format = flags.String("format", "", "format of file to translate")
		label  = flags.String("label", "", "optional: where data fits in collection")
		inPath = flags.String("in", "", "path to input file")
	)

	verbose("parsing args %v", args)
	if err := flags.Parse(args); err != nil {
		return err
	}

	if *format == "" {
		return errors.New("missing format flag")
	}

	if *inPath != "" {
		f, err := os.Open(*inPath)
		if err != nil {
			return err
		}
		defer f.Close()
		src = f
	}

	m := &msg{Label: *label}

	d, err := getDecoder(src, *format)
	if err != nil {
		return err
	}
	e := gob.NewEncoder(dst)

	for i := 0; ; i++ {
		verbose("decoding %d-th value", i)
		if err := d.Decode(&m.Data); err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		verbose("encoding to gob format: %+v", m.Data)
		if err := e.Encode(m); err != nil {
			return err
		}
	}

	return nil
}

type decoder interface {
	Decode(v interface{}) error
}

func getDecoder(src io.Reader, format string) (decoder, error) {
	switch format {
	case "json":
		return json.NewDecoder(src), nil
	case "yaml", "yml":
		return yaml.NewDecoder(src), nil
	case "yaml+md", "yml+md", "yaml+markdown", "yml+markdown":
		return &yamlmdDecoder{src}, nil
	case "raw":
		return &rawDecoder{src}, nil
	default:
		return nil, fmt.Errorf("unknown format type %q", format)
	}
}

type rawDecoder struct {
	io.Reader
}

func (rd rawDecoder) Decode(v interface{}) error {
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Ptr || rv.IsNil() {
		return fmt.Errorf("cannot decode to %T", v)
	}

	raw, err := ioutil.ReadAll(rd.Reader)
	if err != nil {
		return err
	}
	if len(raw) == 0 {
		return io.EOF
	}

	reflect.Indirect(rv).Set(reflect.ValueOf(string(raw)))
	return nil
}

type yamlmdDecoder struct {
	src io.Reader
}

type yamlmd struct {
	Frontmatter interface{}
	Body        string
}

func (md *yamlmdDecoder) Decode(v interface{}) error {
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Ptr || rv.IsNil() {
		return fmt.Errorf("cannot decode to %T", v)
	}

	data := &yamlmd{}

	// first, decode the yaml frontmatter
	dec := yaml.NewDecoder(md.src)
	if err := dec.Decode(&data.Frontmatter); err != nil {
		return err
	}
	if err := dec.Decode(&data.Body); err != nil {
		return err
	}

	reflect.Indirect(rv).Set(reflect.ValueOf(data))

	return nil
}

func init() {
	gob.Register(map[string]interface{}{})      // needed for json
	gob.Register(map[interface{}]interface{}{}) // needed for yaml
	gob.Register([]interface{}{})               // needed for yaml
	gob.Register(yamlmd{})
}
