package main

import (
	"encoding/gob"
	"errors"
	"flag"
	"fmt"
	"io"
)

func reduce(args []string, src io.Reader, dst io.Writer) error {
	if len(args) < 1 {
		return errors.New("reduce is missing subcommand")
	}

	switch subcmd := args[0]; subcmd {
	case "map":
		return reduceMap(args[1:], src, dst)
	default:
		return fmt.Errorf("invalid subcommand for reduce: %q", subcmd)
	}
}

// reduceMap will collect all messages from STDIN and assign them a location in
// a map by the specified label. If no label exists, or a duplicate label is
// given, an error will be returned.
func reduceMap(args []string, src io.Reader, dst io.Writer) error {
	var (
		flags = flag.NewFlagSet("reduce", flag.ContinueOnError)
		label = flags.String("label", "", "optional: where data fits in collection")
	)

	if err := flags.Parse(args); err != nil {
		return err
	}

	rMap := map[string]interface{}{}

	for {
		m := new(msg)
		if err := gob.NewDecoder(src).Decode(m); err != nil {
			if err == io.EOF {
				break
			}
			return fmt.Errorf("unable to decode gob: %w", err)
		}
		verbose("reduceMap decoded %+v", m)

		if m.Label == "" {
			return errors.New("missing label for message")
		}

		_, ok := rMap[m.Label]
		if ok {
			return fmt.Errorf("duplicate label %q", m.Label)
		}

		rMap[m.Label] = m.Data
	}

	return gob.NewEncoder(dst).Encode(msg{
		Label: *label,
		Data:  rMap,
	})
}
