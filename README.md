# [**exos**](https://gitlab.com/pokstad1/exos)keleton static site compiler

`exos` is a static site compiler for those who want a minimal site compiler
with exposed parts. It can be used in conjunction with a scripting language
that allows for the chaining of various commands to construct rendered files
or streams.

## Install

Installing `exos` from source requires Go v1.13 or higher:

`go get -u gitlab.com/pokstad1/exos`

The resulting binary will be located at `$GOPATH/bin/exos`, so make sure you
add that directory to your `$PATH` for easy access.

## Examples

The static site for this project is created using the `exos` tool and a
[Makefile](https://gitlab.com/pokstad1/exos/blob/master/Makefile). See the
templates in
[`examples/website`](https://gitlab.com/pokstad1/exos/tree/master/examples/website).

See the rendered product at https://pokstad1.gitlab.io/exos.

## Translate

Before a data source can be used, it needs to be translated into a common format
understood by the renderer. The translator accepts a range of formats and
converts them into a [gob](https://golang.org/pkg/encoding/gob/) encoded
message. These gob messages are understood by the renderer. 

The following formats are supported:

- `yaml` or `yml`
- `json`
- `yaml+md` - yaml front matter with markdown body (Jekyll style)
   - `Frontmatter` - top level field contains all YAML data
   - `Body` - top level field contains unprocessed markdown content
- `raw` is simply a string containing the unprocessed contents of the source

For example:

`echo '{"a":1}' | exos translate -format json`

Refer to the [`TestTranslateRender` tests](main_test.go) for the exhaustive
list with examples.

Additionally, a translated message can associate a label when used with the
reduce command:

```
exos translate -format json -label A
```

## Reduce

Sometimes you have many data sources you want to merge into a single data
source. The reduce subcommand has some helpful functions for common
transformations:

- `map` - allows multiple data inputs to be mapped to a map key/value. The
   - e.g. `exos reduce map`

All reduce subcommands allow a label to be added so that reduce commands can
be chained together. For example:

`exos reduce map -label abc`

## Render

The render subcommand allows the template language to be used to render content.
For example:

`echo '{"a":1}' | exos translate -format json | exos render -templates 'testdata/*.template' -template example.template`

### Templating Language

The Go standard library `text/template` package is used for the default
templating language. The documentation for the package should be followed for
proper usage: https://golang.org/pkg/text/template/#pkg-overview

Additionally, `exos` provides custom functions for use in templates. These
functions provide useful transformations:

- `markdown` - renders markdown string to HTML string
- `godocToHtml` - renders Go doc comment string to HTML

Note: the complete list of transformations can be shown via the command
`go doc -u gitlab.com/pokstad1/exos funcMap`.
